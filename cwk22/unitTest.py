import os
import unittest
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from app import app, models, db
from datetime import date

class TestCase(unittest.TestCase):
    # Sets up the test environment
    def setUp(self):
        app.config.from_object('config')
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = app.config['TEST_DATABASE_URI']
        self.app = app.test_client()
        db.create_all()
        pass

    # Tears down the test envionment
    def tearDown(self):
        db.session.remove()
        db.drop_all()

    
    # Test 1: Tests the query the program uses to retrieve information about a team
    def test_addTeam(self):
        #team = models.Teams(Name='England', groupCat='B', groupPos=1)
        #db.session.add(team)
        #db.session.commit()

        result = models.Teams.query.filter_by(Name='England').first()

        self.assertEqual(result.Name, 'England')
        self.assertEqual(result.groupCat, 'B')

    # Test 2: Tests retrieving information about a user, this is used for log in pages
    def test_addUser(self):
        user = models.User(firstname='test', surname='account', email='test@gmail.com', password='testPassword')
        db.session.add(user)
        db.session.commit()

        userQ = models.User.query.filter_by(email='test@gmail.com').first()
        self.assertEqual(userQ.email, 'test@gmail.com')
        self.assertEqual(userQ.firstname, 'test')
        self.assertEqual(userQ.password, 'testPassword')

    # Test 3: Tests retrieving information on a players team, use
    def test_userTeam(self):
        user = models.User(firstname='test', surname='account', email='test@gmail.com', password='testPassword', country=1)
        team = models.Teams(Name='England', groupCat='B', groupPos=1)
        db.session.add(team)
        db.session.add(user)
        db.session.commit()

        resultUser = models.User.query.filter_by(email='test@gmail.com').first()
        resultTeam = models.Teams.query.filter_by(teamId=resultUser.country).first()
        
        self.assertEqual(resultTeam.Name, 'England')
        self.assertEqual(resultUser.country, 1)
        self.assertEqual(resultUser.email, 'test@gmail.com')
        self.assertEqual(resultTeam.groupCat, 'B')

    # Test 4: Tests the code for retrieving information on a specific player on a team
    def test_playerTeam(self):
        team = models.Teams(Name='Netherlands', groupCat='A', groupPos=1)
        player = models.Player(firstname='virgil', surname='van-dijk', age=27, country=1)
        db.session.add(team)
        db.session.add(player)
        db.session.commit()

        resultPlayer = models.Player.query.filter_by(surname='van-dijk').first()
        resultTeam = models.Teams.query.get(resultPlayer.country)

        self.assertEqual(resultPlayer.country, 1)
        self.assertEqual(resultTeam.Name, 'Netherlands')
    
    # Test 5: When adding a user to the database that has the same email, an error should be caught
    def test_uniqueEmail(self):
        with self.assertRaises(IntegrityError):
            user = models.User(firstname='test', surname='account', email='test@gmail.com', password='testPassword', country=1)
            userTwo = models.User(firstname='test', surname='account', email='test@gmail.com', password='testPassword', country=1)
            db.session.add(user)
            db.session.add(userTwo)
            db.session.commit()

    # Test 6: Querying a match result
    def test_retrieveMatch(self):
        match = models.Match(teamA='England', teamB='France', aScore=1, bScore=2, date=date.today())
        db.session.add(match)
        db.session.commit()

        resultMatch = models.Match.query.filter_by(teamA='England').first()

        self.assertEqual(resultMatch.teamB, 'France')
        self.assertEqual(resultMatch.aScore, '1')

    # Test 7: Test the route to /addResult
    def test_addResultRoute(self):
        response = self.app.get('/addResult', follow_redirects=True)

        self.assertEqual(response.status_code, 200)
    
    # Test 8: Test the route to /addResult
    def test_loginRoute(self):
        response = self.app.get('/login', follow_redirects=True)

        self.assertEqual(response.status_code, 200)

    # Test 9: Test that someone not logged in can't access account
    def test_loginRequired(self):
        response = self.app.get('/account', follow_redirects=False)

        self.assertEqual(response.status_code, 401)

    # Test 10: BOUNDARY TEST for adding match
    def test_boundaryMatch(self):
        match = models.Match(teamA='England', teamB='France', aScore=0, bScore=2, date=date.today())
        db.session.add(match)
        db.session.commit()

        resultMatch = models.Match.query.filter_by(teamA='England').first()

        self.assertEqual(resultMatch.teamB, 'France')
        self.assertEqual(resultMatch.aScore, '0')

if __name__=='__main__':
    unittest.main()