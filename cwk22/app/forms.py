from flask_wtf import FlaskForm
from wtforms import StringField, DateField, PasswordField, SelectField, IntegerField, SelectMultipleField
from wtforms.validators import DataRequired, Length, Email, EqualTo, NumberRange
from app import models

class SignUpForm(FlaskForm):
    firstname = StringField('firstname', [Length(min=2, max=250), DataRequired()])
    surname = StringField('surname', [Length(min=2, max=250), DataRequired()])
    email = StringField('Email', [Length(min=4, max=64), DataRequired(), Email(message='Please enter a valid email address.')])
    password = PasswordField('Password', [Length(min=6,max=35), DataRequired()])
    cPassword = PasswordField('Confirm Password', [Length(min=6,max=35), DataRequired(), EqualTo('password')])
    team =  SelectField('teams', choices=[(m.teamId, m.Name) for m in models.Teams.query.all()])

class LoginForm(FlaskForm):
    email = StringField('Email', [Length(min=4, max=64), DataRequired(), Email(message='Please enter a valid email address.')])
    password = PasswordField('Password', [Length(min=6,max=35), DataRequired()])

class ChangePasswordForm(FlaskForm):
    password = PasswordField('Password', [Length(min=6,max=35), DataRequired()])
    cPassword = PasswordField('Confirm Password', [Length(min=6,max=35), DataRequired(), EqualTo('password')])

class ChangeTeamForm(FlaskForm):
    team =  SelectField('teams', choices=[(m.teamId, m.Name) for m in models.Teams.query.all()])

class AddResultForm(FlaskForm):
    teamA =  SelectField('teams', choices=[(m.teamId, m.Name) for m in models.Teams.query.all()])
    teamB =  SelectField('teams', choices=[(m.teamId, m.Name) for m in models.Teams.query.all()])
    aScore = IntegerField('aScore', [NumberRange(min=0)])
    bScore = IntegerField('aScore', [NumberRange(min=0)])
    date = DateField('date', [DataRequired()])

class AddPlayerForm(FlaskForm):
    firstname = StringField('firstname', [Length(min=2, max=250), DataRequired()])
    surname = StringField('surname', [Length(min=2, max=250), DataRequired()])
    team =  SelectField('teams', choices=[(m.teamId, m.Name) for m in models.Teams.query.all()])
    age = IntegerField('age', [NumberRange(min=0)])
    choices = [(p.positionId, p.Name) for p in models.Position.query.order_by('positionId')]
    positions =  SelectMultipleField('position', coerce=int, choices=choices)