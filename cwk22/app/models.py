from app import db
from flask_login import UserMixin

PlayerPosition = db.Table('PlayerPosition', db.Model.metadata,
    db.Column('playerId', db.Integer, db.ForeignKey('player.playerId')),
    db.Column('positionId', db.Integer, db.ForeignKey('position.positionId'))
)

class Player (db.Model):
    playerId = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(250), index=True)
    surname = db.Column(db.String(250),index=True)
    age = db.Column(db.Integer)
    positions = db.relationship('Position',secondary=PlayerPosition)
    country = db.Column(db.Integer, db.ForeignKey('teams.teamId'))

    def __repr__(self):
        return  self.firstname + ' ' + self.surname

class Position (db.Model):
    positionId = db.Column(db.Integer,primary_key=True)
    Name = db.Column(db.String(50), index=True)
    location = db.Column(db.String(50), index=True)
    players = db.relationship('Player',secondary=PlayerPosition, overlaps="positions")
    
    def __repr__(self):
        return  self.Name

class Teams (db.Model): #Staff
    teamId = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(250), index=True)
    groupCat = db.Column(db.String(1), index=True)
    groupPos = db.Column(db.Integer, index=True)
    players = db.relationship('Player', backref='teams', lazy='dynamic')
    users = db.relationship('User', backref='user', lazy='dynamic')

    def __repr__(self):
        return self.Name


class Match (db.Model):
    matchId = db.Column(db.Integer, primary_key=True)
    teamA = db.Column(db.String(250), nullable=False)
    teamB = db.Column(db.String(250), nullable=False)
    aScore = db.Column(db.String(4))
    bScore = db.Column(db.String(4))
    date = db.Column(db.DateTime)


    def __repr__(self):
        return  self.result


class User (UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(250), index=True)
    surname = db.Column(db.String(250),index=True)
    email = db.Column(db.String(64), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    country = db.Column(db.Integer, db.ForeignKey('teams.teamId'))

    def __repr__(self):
        return self.firstname
