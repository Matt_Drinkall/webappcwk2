from flask import render_template, flash, request, redirect, url_for
from app import app, models, db, admin
from .forms import LoginForm, SignUpForm, ChangePasswordForm, ChangeTeamForm, AddResultForm, AddPlayerForm
from flask_admin.contrib.sqla import ModelView
from sqlalchemy.exc import IntegrityError
import datetime
from flask_login import LoginManager, login_user, login_required, logout_user, current_user

admin.add_view(ModelView(models.Position, db.session))
admin.add_view(ModelView(models.Player, db.session))
admin.add_view(ModelView(models.Teams, db.session))
admin.add_view(ModelView(models.Match, db.session))
admin.add_view(ModelView(models.User, db.session))

login_manager = LoginManager()
login_manager.init_app(app)

@app.route('/', methods=['GET', 'POST'])
def home():
    app.logger.info('home route requested')
    matches = models.Match.query.all()
    app.logger.info("Match database has been successfully queried")
    return render_template('home.html', title='Qatar World Cup 2022', matches=matches, len=len(matches))

@app.route('/teams/<id>/players', methods=['GET'])
def getPlayersonTeam(id):
    app.logger.info("player route requested for team id %s", id)
    team = models.Teams.query.filter_by(teamId=id).first()
    app.logger.info("Teams database has been successfully queried for %s", team.Name)
    return render_template('players.html', title='World Cup Players', players=team.players, team=team.Name)

@app.route('/myTeam', methods=['GET'])
def myTeam():
    app.logger.info('myTeam route requested')
    if not(current_user.is_authenticated):
        app.logger.info('User is required to login, redircting to the login page')
        return redirect(url_for('login'))

    app.logger.info('User is logged in')
    teamId = current_user.country
    team = models.Teams.query.filter_by(teamId=teamId).first()
    app.logger.info("Teams database has been successfully queried")

    try:
        matches = models.Match.query.filter_by(teamA=team.Name).all()
        app.logger.info("Match database queried and successfully finds match for teamA")
    except AttributeError:
        try:
            matches = models.Match.query.filter_by(teamB=team.Name).all()
            app.logger.info("Match database queried and successfully finds match for teamB")
        except AttributeError:
            app.logger.error("Match database does not contain a match for %s", team.Name)
            return 'Attribute Error'

    return render_template('myTeam.html', title='World Cup MyTeam', players=team.players, team=team.Name, matches=matches)

@app.route('/addPlayer', methods=['GET', 'POST'])
def addPlayer():
    if not current_user.is_authenticated:
        flash('You must be logged in to add a player', 'error')
        return redirect(url_for('login'))

    form = AddPlayerForm()

    if form.validate_on_submit():
        posList = []
        for posId in form.positions.data:
            posList.append(models.Position.query.get(posId))

        pl = models.Player(firstname=form.firstname.data, surname=form.surname.data, age=form.age.data, positions=posList, country=form.team.data)
        db.session.add(pl)
        db.session.commit()
        app.logger.info('Player added')
        flash('Player successfully added', 'success')
    
    return render_template('addPlayer.html', title='Add Player', form=form)

@app.route('/results', methods=['GET'])
def results():
    app.logger.info('results route requested')
    matches = models.Match.query.all()
    app.logger.info("Match database queried for all values")
    dateList = [matches[0].date]
    for match in matches:
        dateList.append(match.date.strftime('%d %B'))
    return render_template('results.html', title='World Cup Results', matches=matches, dates=dateList)

@app.route('/addResult', methods=['GET', 'POST'])
def addResult():
    if not current_user.is_authenticated:
        flash('You must be logged in to add a result', 'error')
        return redirect(url_for('login'))

    form = AddResultForm()
    if form.validate_on_submit():
        teamA = models.Teams.query.filter_by(teamId=form.teamA.data).first()
        teamB = models.Teams.query.filter_by(teamId=form.teamB.data).first()

        # Checks that the user hasn't entered the same team
        if(teamA.teamId == teamB.teamId):
            app.logger.info('User entered same team')
            flash('You cannot add a match where both teams are the same', 'error')
        else:
            match=models.Match(teamA=teamA.Name, teamB=teamB.Name, aScore=form.aScore.data, bScore=form.bScore.data, date=form.date.data)
            db.session.add(match)
            db.session.commit()
            app.logger.info('Match added')
            flash('Result successfully added', 'success')
    return render_template('addResult.html', title='Add Result', form=form)

@app.route('/teams', methods=['GET', 'POST'])
def teams():
    app.logger.info('teams route requested')
    teams = models.Teams.query.all()

    app.logger.info("Teams database queried for all values")
    return render_template('teams.html', title='World Cup Teams', teams=teams)

@app.route('/account', methods=['GET', 'POST'])
@login_required
def account():
    form = ChangePasswordForm()
    formT = ChangeTeamForm()
    app.logger.info('account route requested, user login is required')
    user = current_user
    team = models.Teams.query.filter_by(teamId=user.country).first()
    #user = models.User.query.filter_by(id=id).first()

    if form.validate_on_submit():
        # Checks if the user has changed their password
        if(form.password.data != form.cPassword.data):
            app.logger.error('Password does not match')
            flash('Password does not match', 'error')
        else:
            user.password=form.password.data
            db.session.add(user)
            db.session.commit()
            app.logger.info('Password Successfully changed')
            flash('Password successfully changed', 'success')
    
    if formT.validate_on_submit():
        user.country=formT.team.data
        db.session.add(user)
        db.session.commit()
        app.logger.info('Team Successfully changed')
        flash('Team successfully changed', 'success')
        return redirect(url_for('account'))

    return render_template('account.html', title='My Account', user=current_user, team=team, form=form, formT=formT)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    app.logger.info('signup route requested')
    # Checks if there is a file already setup with that email
    form = SignUpForm()
    try:
        if form.validate_on_submit():
            p = models.User(firstname=form.firstname.data, surname=form.surname.data, email=form.email.data,
            password=form.password.data, country=form.team.data)
            db.session.add(p)
            db.session.commit()
            user = models.User.query.filter_by(email=form.email.data, password=form.password.data).first()
            app.logger.info('User successfully signed up')
            login_user(user)
            return redirect(url_for('account'))

    # Runs when the user has commited a duplicated item
    except IntegrityError:
        db.session.rollback()
        app.logger.error('Sign Up email is already used')
        flash('Email already signed up', 'error')

    return render_template('signup.html', title='SignUp', form=form)

@login_manager.user_loader
def load_user(userId):
    return models.User.query.get(int(userId))

@app.route('/login', methods=['GET', 'POST'])
def login():
    app.logger.info('login route requested')
    if current_user.is_authenticated:
        return redirect(url_for('account'))

    form = LoginForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data

        user = models.User.query.filter_by(email=email, password=password).first()
        if user:
            app.logger.info('User successfully logged in')
            login_user(user)
            return redirect(url_for('account'))
        else:
            app.logger.error('User gave wrong log in')
            flash('Invalid username or password', 'error')
    return render_template('login.html', title='LogIn', form=form)

@app.route('/logout')
def logout():
    app.logger.info('logout route requested')
    logout_user()
    app.logger.info('User successfully logged out')
    return redirect(url_for('login'))
